from time import sleep, time

import RPi.GPIO as GPIO

#constants
DEBUG = False

CALIBRATIONS = 5
CALIBRATION_DELAY = 1
TRIGGER_TIME = 0.00001
SPEED_OF_SOUND = 343
SETTLE_TIME = 5
VALUES = []

GPIO.setmode(GPIO.BCM)

#pins

trig = 18
echo = 27

GPIO.setup(trig,GPIO.OUT)
GPIO.setup(echo,GPIO.OUT)

#helperFunctions

def calibrate():
    print("Calibrating...\n  --Place sensor a measured place from obstacle.")
    knownDistance = input("  >>Distance: ")

    print("  --Getting Calibration Measurements")
    dist_average = 0

    for i in range(CALIBRATIONS):
        print(range(CALIBRATIONS))
        distance = getDistance()
        if(DEBUG):
            print("  -- Got %scm"%distance)
        dist_average += distance
        sleep(CALIBRATION_DELAY)

    dist_average /= CALIBRATIONS

    if(DEBUG):
        print("  --Average is %s"%dist_average)

    correctionFactor = knownDistance / dist_average

    if(DEBUG):
        print("  --Correction Factor is %s"%correctionFactor)

    print("--Done!")

    return(correctionFactor)
    
def getDistance():
    print("Getting Distance")
    GPIO.output(trig,GPIO.HIGH)
    sleep(TRIGGER_TIME)
    GPIO.output(trig,GPIO.LOW)

    print("before")
    print("{}".format(GPIO.input(echo)))
    while(GPIO.input(echo) == GPIO.LOW): 
        start = time()
    print("middle")
    while(GPIO.input(echo) == GPIO.HIGH): 
        stop = time()
    print("after")
    duration = start - stop
    distance = duration * SPEED_OF_SOUND



    distance /= 2
    distance *= 100
    return(distance)

def manualsort(lister):
    for index in range(1,len(lister)):

     currval = lister[index]
     position = index

     while(lister[position-1]>currval):
         lister[position] = lister[position-1]
         position = position-1

     lister[position]=currval

print("Waiting for sensor to settle (%ss)"%SETTLE_TIME)
GPIO.output(trig, GPIO.LOW)
sleep(SETTLE_TIME)

correctionFactor = calibrate()

input("Press enter to begin")
print("Gettng measurements")

while(True):
    print("  --Measuring...")
    distance = getDistance() * correctionFactor
    sleep(1)
    distance = round(distance, 4)
    print("Distance is: %scm"%distance)
    i = input("Get Another measurement? [Y/n]")
    VALUES.append(distance)
    if(not i in ["y","Y",""]):
        break

print("done")
print(VALUES)
manualsort(VALUES)
print(VALUES)
GPIO.cleanup()
