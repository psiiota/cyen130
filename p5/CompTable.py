##########################################################################################
# Name: Andrew Malsbury
# Date: October 29, 2018
# Description: Search table outputter
##########################################################################################
# Imports:
#
import math
##########################################################################################
def tTable():
    print("-"*50)
    print("n\tseq\tbin\tperf")
    print("-"*50)
    n = minimum
    while(n <= maximum):
        seqperf = sequentialCal(n)
        binperf = binaryCal(n)
        perfdiff = round(seqperf/binperf)
        print("%s\t%s\t%s\t%s"%(n,seqperf,binperf,perfdiff))
        n += interval

def sequentialCal(size):
    avg = math.ceil(size/2)
    return(avg)

def binaryCal(size):
    avg = math.floor(math.log(size,2)) + 1
    return(avg)

###############################################
# MAIN PART OF THE PROGRAM
###############################################
# get user input for the minimum (make sure that it is >= 0)
usr = int(input("Enter Minimum Value:\n>>> "))
while(usr < 1):
    usr = int(input("###-ERROR-### Please input a number that is >= 0.\n>>>"))
minimum = usr

# get user input for the maximum (make sure that is is >= minimum)
usr = int(input("Enter Maximum Value:\n>>> "))
while(usr < minimum):
    usr = int(input("###-ERROR-### Please input a number that is >= %s.\n>>>"%minimum))
maximum = usr

# get user input for the interval (make sure that it is >= 1)
usr = int(input("Enter Interval:\n>>> "))
while(usr < 1):
    usr = int(input("###-ERROR-### Please input a number that is >= 1.\n>>>"))
interval = usr

# generate the table
tTable()
