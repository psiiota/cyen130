###########################################################################################
# Name: Andrew Malsbury
# Date: 10-22-2018
# Description: LED the Way
###########################################################################################
import RPI.GPIO as GPIO
from time import sleep

led = 17 #Output PIN
button = 25 #Input PIN

GPIO.setmode(GPIO.BCM)

GPIO.setup(led, GPIO.OUT) # Setting Output mode
GPIO.setup(button, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) # Setting input mode


##################################################################################################
#
# MAIN PROGRAM
#
###################################################################################################

while True:
    if (GPIO.input(button) == GPIO.HIGH):
        GPIO.output(led, GPIO.HIGH)
        sleep(0.1)
        GPIO.output(led, GPIO.LOW)
        sleep(0.1)
    else:
        GPIO.output(led, GPIO.HIGH)
        sleep(0.5)
        GPIO.output(led, GPIO.LOW)
        sleep(0.5)